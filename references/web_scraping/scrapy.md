1. Start a project by "scrapy startproject ourfirstscraper"
2. Cd to ourfirstscraper
3. scrapy genspider redditbot www.reddit.com/r/gameofthrones/
4. scrapy shell (for shell)
    a. fetch("https://www.reddit.com/r/gameofthrones/")
    b. print(response.text)
5. response.css("time::attr(title)").extract() -> Extract the attribute value
6. scrapy shell
    a. we can use fetch to help us to send http request and get the response for us.
    b. fetch("http://quotes.toscrape.com/") 
    c. You can get the detail of the HTTP response by accessing property of the response object.
    d. response.xpath("//div[@class='quote']/span[@class='text']").extract()
    e. response.xpath("//div[@class='quote']/span[@class='text']/text()").extract()
    