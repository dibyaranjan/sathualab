import selenium
import time

options = webdriver.ChromeOptions()
options.add_argument('--ignore-certificate-errors')
options.add_argument('--incognito')
options.add_argument('--headless')
driver = webdriver.Chrome("../lib/chromedriver", options=options)

driver.get(URL)
time.sleep(1)
response = driver.page_source
driver.close()