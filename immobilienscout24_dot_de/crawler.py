"""
File:           crawler.py
Author:         Dibyaranjan Sathua
Website:        sathualab.com
Contact:        +91 9040340870
Created on:     11/07/20, 11:40 pm

This is the main code which will crawl a given link and fetch real estate data.
Framework Used: Selenium
Driver Used and Tested: Chrome Driver version 84
Python: 3.8
"""
import logging
import csv
import sys
from typing import Optional, List
import time

from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select, WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import NoSuchElementException

from data import CrawledData


class ErrorConstant:
    NotFound = "Not Found"


class SathuaLabCrawler:
    """ Crawl different pages for immobilienscout24.de and extract real estate data """
    START_URL = "https://www.immobilienscout24.de/"

    def __init__(self, city: str = None):
        """ Initialize variables """
        self.city = city
        self.options = webdriver.ChromeOptions()
        self.options.add_argument('--ignore-certificate-errors')
        self.options.add_argument('--incognito')
        self.options.add_argument('--headless')
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(logging.INFO)
        handler = logging.StreamHandler()
        formatter = logging.Formatter(
            fmt="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
            datefmt="%d-%b-%Y %H:%M:%S"
        )
        handler.setFormatter(formatter)
        self.logger.addHandler(handler)
        self.driver: Optional[webdriver.Chrome] = None
        self.property_links = []
        self.crawled_data: List[CrawledData] = []
        self.start_time = None

    def __enter__(self):
        """ Context manager enter function """
        self.start_time = time.time()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        """ Context manager exit function """
        total_time = time.strftime(
            "%H hours %M mins %S secs",
            time.gmtime(time.time() - self.start_time)
        )
        self.logger.info(f"Total time taken: {total_time}")
        if self.driver:
            self.driver.close()

    def crawl(self):
        self.driver = webdriver.Chrome(
            executable_path="./lib/chromedriver",
            options=self.options
        )
        self.driver.implicitly_wait(1)
        self.driver.get(self.START_URL)
        # Filing the filter form to get the result page
        self._parse_main_page()

    def _parse_main_page(self):
        """ Set the filter in the main page and get the result link """
        self.logger.info(f"Crawling main page: {self.driver.current_url}")
        if self.city is None:
            self.logger.critical(f"No city name provided.")
            sys.exit(1)

        # Selecting "to buy" in the filter
        marketing_type = Select(self.driver.find_element_by_name("marketingType"))
        # Select by value
        marketing_type.select_by_value("BUY")
        self.logger.info("Marketing type: Buy")
        # Can also use the CSS selector with the id of the element fruits01
        # self.driver.find_element_by_css_selector("#fruits01 [value='1']").click()

        # Input the location
        location = self.driver.find_element_by_id("oss-location")
        location.send_keys(self.city)
        self.logger.info(f"Location: {self.city}")
        # Select from the autocomplete list
        autocomplete_list = self.driver.find_element_by_id("ui-id-1")
        first_element = autocomplete_list.find_element_by_css_selector(
            "li.ui-menu-item"
        )
        # Sometimes this will throw an exception
        # selenium.common.exceptions.ElementClickInterceptedException:
        # Message: element click intercepted: Element
        # <div class="ui-menu-item-wrapper ui-state-active">...</div> is not clickable at
        # point (324, 418)
        # first_element.click()
        # Using execute_script to click the button
        self.driver.execute_script("arguments[0].click();", first_element)
        time.sleep(1)

        # Select the real estate type
        real_estate_type = Select(self.driver.find_element_by_id("oss-real-estate-type-buy"))
        real_estate_type.select_by_value("APARTMENT_BUY")
        self.logger.info("Real estate type: Apartment")

        # Click the result button
        search_btn = self.driver.find_element_by_css_selector(
            ".oss-main-criterion.oss-button.button-primary.one-whole"
        )
        spans = search_btn.find_elements_by_css_selector("span")
        self.logger.info(" ".join([x.text for x in spans]))
        search_btn.click()
        # Parse the result page which get loaded after clicking the search button
        self._parse_result_page()

    def _parse_result_page(self):
        """ Extract links to each property page """
        # Wait for the page to load
        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.ID, "pageSelection")))

        self.logger.info(f"Crawling result page: {self.driver.current_url}")
        # Print the header information
        search_head = self.driver.find_element_by_id("searchHead")\
            .find_element_by_css_selector("h1")
        self.logger.info(f"Search Head: {search_head.text}")
        property_headings = self.driver.find_elements_by_css_selector(
            "div#listings ul#resultListItems li.result-list__listing"
        )

        self.property_links = [
            x.find_element_by_css_selector(
                "a.result-list-entry__brand-title-container"
            ).get_attribute("href")
            for x in property_headings
        ]

        # Find the link for next page to crawl
        try:
            next_link_element = self.driver.find_element_by_id("pager")\
                .find_element_by_css_selector("a[data-is24-qa='paging_bottom_next']")
            next_link = next_link_element.get_attribute("href")
        except NoSuchElementException:
            self.logger.info("Last page reached")
            next_link = None

        # Extract data from each property listing
        for link in self.property_links:
            self.driver.get(link)
            time.sleep(1)
            data = self._parse_property_page()
            if data:
                self.crawled_data.append(data)

        if next_link:
            self.driver.get(next_link)
            self.logger.info("Navigating to Next Page")
            # self.driver.execute_script("arguments[0].click();", next_link_element)
            self._parse_result_page()

    def _parse_property_page(self):
        """ Extract data from the property page """
        # Some of the pages looks different
        self.logger.info(f"Crawling property page: {self.driver.current_url}")
        try:
            main_property_section = self.driver.find_element_by_css_selector(
                "div.is24-ex-details.main-criteria-headline-size.two-column-layout"
            )
        except NoSuchElementException:
            self.logger.warning("This page can not be crawled")
            return None

        try:
            name = main_property_section.find_element_by_css_selector("div.criteriagroup h1")
            name = name.text.strip()
        except NoSuchElementException:
            self.logger.warning("Name not found")
            name = ErrorConstant.NotFound

        try:
            address = main_property_section.find_elements_by_css_selector(
                "div.address-block span"
            )
            address = [x.text for x in address]
            address = "\n".join(address)
        except NoSuchElementException:
            self.logger.warning("Address Not Found")
            address = ErrorConstant.NotFound

        # Purpase price section
        try:
            purchase_price = main_property_section.find_element_by_css_selector(
                "div.mainCriteria.flex-item "
                "div.is24qa-kaufpreis.is24-value.font-semibold.is24-preis-value"
            )
            purchase_price = purchase_price.text.strip()
        except NoSuchElementException:
            self.logger.warning("Purchase Price Not Found")
            purchase_price = ErrorConstant.NotFound

        try:
            monthly_price = main_property_section.find_element_by_css_selector(
                "span.monthly-rate-result.monthly-rate-value.padding-top-xs"
            )
            monthly_price = monthly_price.text.strip()
        except NoSuchElementException:
            self.logger.warning("Monthly Price Not Found")
            monthly_price = ErrorConstant.NotFound

        try:
            room = main_property_section.find_element_by_css_selector(
                "div.mainCriteria.flex-item div.is24qa-zi.is24-value.font-semibold"
            )
            room = room.text.strip()
        except NoSuchElementException:
            self.logger.warning("Room Not Found")
            room = ErrorConstant.NotFound

        try:
            living_space = main_property_section.find_element_by_css_selector(
                "div.mainCriteria.flex-item div.is24qa-wohnflaeche-ca.is24-value.font-semibold"
            )
            living_space = living_space.text.strip()
        except NoSuchElementException:
            self.logger.warning("Living Space Not Found")
            living_space = ErrorConstant.NotFound

        # Provider name and phone number section
        try:
            provider_name = main_property_section.find_element_by_css_selector(
                "div.style__truncateChild___2Z9XG.font-semibold"
            )
            provider_name = provider_name.text.strip()
        except NoSuchElementException:
            self.logger.warning("Provider Name Not Found")
            provider_name = ErrorConstant.NotFound

        try:
            phone_no_button = main_property_section.find_element_by_css_selector(
                "button.link-text.padding-none.palm-hide.style__realtorTelephoneNumber___CwmJv")
            phone_no_button.send_keys(Keys.RETURN)
            phone_no_pop_up = self.driver.find_element_by_id("is24-expose-popup")
            phone_no = phone_no_pop_up.find_element_by_xpath(
                ".//ul[@data-qa='contactPhoneNumbers']//span[@class='font-semibold']"
            )
            phone_no = phone_no.text.strip()
        except NoSuchElementException:
            self.logger.warning("Phone No Not Found")
            phone_no = ErrorConstant.NotFound

        # Property details section
        try:
            amenities = main_property_section.find_elements_by_css_selector(
                "div.criteriagroup.boolean-listing.padding-top-l span.palm-hide"
            )
            amenities = [x.text for x in amenities]
            amenities = ", ".join(amenities)
        except NoSuchElementException:
            self.logger.warning("Amenities Not Found")
            amenities = ErrorConstant.NotFound

        try:
            property_section = main_property_section.find_element_by_css_selector(
                "div.criteriagroup.criteria-group--two-columns"
            )
            property_details = {}
            for info in property_section.find_elements_by_css_selector("dl"):
                property_details[info.find_element_by_css_selector("dt").text] = \
                    info.find_element_by_css_selector("dd").text

            property_details = ", ".join(
                [f"{key}: {value}" for key, value in property_details.items()]
            )
        except NoSuchElementException:
            self.logger.warning("Property details Not Found")
            property_details = ErrorConstant.NotFound

        # Cost section
        # There are two div with class grid. But we are interested in the first one
        # Select the div element which is next sibling to h4 element with
        # data-qa=is24qa-kosten-label
        # + is used for next or adjacent sibling
        try:
            cost_section = main_property_section.find_element_by_css_selector(
                "h4[data-qa='is24qa-kosten-label']+div.criteriagroup.one-whole div.grid"
            )
            cost_details = {}
            for info in cost_section.find_elements_by_css_selector("dl"):
                cost_details[info.find_element_by_css_selector("dt").text] = \
                    info.find_element_by_css_selector("dd").text

            interested_cost_details = ["Hausgeld", "Provision für Käufer"]
            house_allowance = cost_details.get("Hausgeld", ErrorConstant.NotFound)
            commission = cost_details.get("Provision für Käufer", ErrorConstant.NotFound)
            cost_details = ", ".join(
                [
                    f"{key}: {value}"
                    for key, value in cost_details.items()
                    if key in interested_cost_details
                ]
            )
        except NoSuchElementException:
            self.logger.warning("Cost details Not Found")
            house_allowance = ErrorConstant.NotFound
            commission = ErrorConstant.NotFound
            cost_details = ErrorConstant.NotFound

        # Building substance and energy certificate
        try:
            building_section = main_property_section.find_element_by_css_selector(
                "div.criteriagroup.criteria-group--border.criteria-group--two-columns."
                "criteria-group--spacing"
            )
            building_details = {}
            for info in building_section.find_elements_by_css_selector("dl"):
                building_details[info.find_element_by_css_selector("dt").text] = \
                    info.find_element_by_css_selector("dd").text

            construction_year = building_details.get("Baujahr", ErrorConstant.NotFound)
            building_details = ", ".join(
                [f"{key}: {value}" for key, value in building_details.items()]
            )
        except NoSuchElementException:
            self.logger.warning("Building information not found")
            construction_year = ErrorConstant.NotFound
            building_details = ErrorConstant.NotFound

        # Location section
        # Use + for adjacent sibling selector
        try:
            location_info = self.driver.find_element_by_css_selector(
                "h4.is24qa-lage-label.padding-top-xl.margin-bottom-s+"
                "div.is24-text.margin-bottom.is24-long-text-attribute"
            ).text
        except NoSuchElementException:
            self.logger.warning("Location Not Found")
            location_info = ErrorConstant.NotFound

        return CrawledData(
            name=name,
            address=address,
            pre_processed_purchase_price=purchase_price,
            pre_processed_monthly_price=monthly_price,
            pre_processed_room=room,
            living_space=living_space,
            provider_name=provider_name,
            provider_phone_no=phone_no,
            amenities=amenities,
            property_details=property_details,
            cost_details=cost_details,
            pre_processed_house_allowance=house_allowance,
            commission=commission,
            building_details=building_details,
            pre_processed_construction_year=construction_year,
            location_info=location_info
        )

    def write_to_csv(self):
        with open("output.csv", mode="w") as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=CrawledData.get_fields())
            writer.writeheader()
            for data in self.crawled_data:
                writer.writerow(data.to_dict())


if __name__ == "__main__":
    with SathuaLabCrawler("Berlin - Mitte (Mitte)") as crawler:
        crawler.crawl()
        crawler.write_to_csv()
