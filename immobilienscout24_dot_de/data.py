"""
File:           data.py
Author:         Dibyaranjan Sathua
Created on:     12/07/20, 6:16 pm

Class to store crawled data.
"""
from dataclasses import dataclass, fields, field


@dataclass()
class CrawledData:
    """ Data class to store crawled data """
    name: str
    address: str
    pre_processed_purchase_price: str = field(metadata={"use_in_csv": False})
    purchase_price: int = field(init=False, default=-1)
    pre_processed_monthly_price: str = field(metadata={"use_in_csv": False})
    monthly_price: int = field(init=False, default=-1)
    pre_processed_room: str = field(metadata={"use_in_csv": False})
    room: int = field(init=False, default=-1)
    living_space: str
    provider_name: str
    provider_phone_no: str
    amenities: str
    property_details: str
    cost_details: str
    pre_processed_house_allowance: str = field(metadata={"use_in_csv": False})
    house_allowance: int = field(init=False, default=-1)
    commission: str
    building_details: str
    pre_processed_construction_year: str = field(metadata={"use_in_csv": False})
    construction_year: int = field(init=False, default=-1)
    location_info: str

    def __post_init__(self):
        try:
            self.purchase_price = self.price_to_int(self.pre_processed_purchase_price)
        except ValueError as err:
            print("Error in converting purchase price")

        try:
            self.monthly_price = self.price_to_int(self.pre_processed_monthly_price)
        except ValueError as err:
            print("Error in converting monthly price")

        try:
            self.house_allowance = self.price_to_int(self.pre_processed_house_allowance)
        except ValueError as err:
            print("Error in converting house allowance")

        try:
            self.room = int(self.pre_processed_room)
        except ValueError as err:
            print("Error in converting room")

        try:
            self.construction_year = int(self.pre_processed_construction_year)
        except ValueError:
            print("Error in converting year")

    @staticmethod
    def price_to_int(value):
        """ Convert string price to int """
        return int(
            value.strip("€").strip().replace(".", "").replace(",", "")
        )

    @staticmethod
    def get_fields():
        """ Return the data class fields name """
        return [
            x.name
            for x in fields(CrawledData)
            if x.metadata.get("use_in_csv", True)
        ]

    def to_dict(self):
        """ Convert data class to dict """
        class_fields = CrawledData.get_fields()
        return {x: getattr(self, x) for x in class_fields}
