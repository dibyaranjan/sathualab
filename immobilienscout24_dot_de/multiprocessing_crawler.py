"""
File:           multiprocessing_crawler.py
Author:         Dibyaranjan Sathua
Created on:     14/07/20, 11:38 pm
"""
import logging
import csv
import sys
from typing import Optional, List
import time
import multiprocessing as mp
from concurrent.futures import ThreadPoolExecutor

from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select, WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import NoSuchElementException

from data import CrawledData

START_URL = "https://www.immobilienscout24.de/"
NO_OF_PROCESS = 4
CITY = "Berlin - Mitte (Mitte)"
RESULT_PAGE_URL = []
RUN_MODE = 1        # 1 for City, 2 for result url (change the value of START_URL)
NOT_FOUND = "Not Found"
CRAWLED_DATA: mp.Queue = mp.Queue()
NO_OF_PAGES = 1
options = webdriver.ChromeOptions()
options.add_argument('--ignore-certificate-errors')
options.add_argument('--incognito')
options.add_argument('--headless')
driver_arguments = dict(
    executable_path="lib/chromedriver_mac",
    options=options
)
driver_process1: webdriver.Chrome = webdriver.Chrome(**driver_arguments)
driver_process1.implicitly_wait(1)
driver_process2: webdriver.Chrome = webdriver.Chrome(**driver_arguments)
driver_process2.implicitly_wait(1)
driver_process3: webdriver.Chrome = webdriver.Chrome(**driver_arguments)
driver_process3.implicitly_wait(1)
driver_process4: webdriver.Chrome = webdriver.Chrome(**driver_arguments)
driver_process4.implicitly_wait(1)

PROCESS_NAME_DRIVER_MAPPING = {
    "process1": driver_process1,
    "process2": driver_process2,
    "process3": driver_process3,
    "process4": driver_process4
}


def sathualab_crawler():
    """ Create a crawler """
    options = webdriver.ChromeOptions()
    options.add_argument('--ignore-certificate-errors')
    options.add_argument('--incognito')
    options.add_argument('--headless')
    driver_arguments = dict(
        executable_path="lib/chromedriver_mac",
        options=options
    )
    driver: webdriver.Chrome = webdriver.Chrome(**driver_arguments)
    driver.implicitly_wait(1)
    driver_process1: webdriver.Chrome = webdriver.Chrome(**driver_arguments)
    driver_process1.implicitly_wait(1)
    driver_process2: webdriver.Chrome = webdriver.Chrome(**driver_arguments)
    driver_process2.implicitly_wait(1)
    driver_process3: webdriver.Chrome = webdriver.Chrome(**driver_arguments)
    driver_process3.implicitly_wait(1)
    driver_process4: webdriver.Chrome = webdriver.Chrome(**driver_arguments)
    driver_process4.implicitly_wait(1)
    subprocess_drivers = [driver_process1, driver_process2, driver_process3, driver_process4]

    # Logger
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.INFO)
    handler = logging.StreamHandler()
    formatter = logging.Formatter(
        fmt="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
        datefmt="%d-%b-%Y %H:%M:%S"
    )
    handler.setFormatter(formatter)
    logger.addHandler(handler)

    start_time = time.time()
    try:
        # Run the code by searching by city name
        if RUN_MODE == 1:
            # Filing the filter form to get the result page
            RESULT_PAGE_URL.append(parse_main_page(driver, logger))

        # Run the code by the result page url (START_URL will be set to the result page url)
        elif RUN_MODE == 2:
            RESULT_PAGE_URL.append(START_URL)

        while RESULT_PAGE_URL:
            page_link = RESULT_PAGE_URL.pop()
            driver.get(page_link)
            parse_result_page(driver, logger, subprocess_drivers)
    except:
        logger.exception("Something went wrong with the crawler.")
    finally:
        total_time = time.strftime(
            "%H hours %M mins %S secs",
            time.gmtime(time.time() - start_time)
        )
        logger.info(f"Total time taken: {total_time}")
        logger.info("Closing all the drivers. Please wait...")
        if driver:
            driver.close()
        if driver_process1:
            driver_process1.close()
        if driver_process2:
            driver_process2.close()
        if driver_process3:
            driver_process3.close()
        if driver_process4:
            driver_process4.close()

        logger.info("Completed !!!")


def parse_main_page(driver: webdriver.Chrome, logger: logging.Logger):
    """ Set the filter in the main page and get the result link """
    driver.get(START_URL)
    logger.info(f"Crawling main page: {driver.current_url}")
    # Selecting "to buy" in the filter
    marketing_type = Select(driver.find_element_by_name("marketingType"))
    # Select by value
    marketing_type.select_by_value("BUY")
    logger.info("Marketing type: Buy")
    # Can also use the CSS selector with the id of the element fruits01
    # self.driver.find_element_by_css_selector("#fruits01 [value='1']").click()

    # Input the location
    location = driver.find_element_by_id("oss-location")
    location.send_keys(CITY)
    logger.info(f"Location: {CITY}")
    # Select from the autocomplete list
    autocomplete_list = driver.find_element_by_id("ui-id-1")
    first_element = autocomplete_list.find_element_by_css_selector(
        "li.ui-menu-item"
    )

    driver.execute_script("arguments[0].click();", first_element)
    time.sleep(1)

    # Select the real estate type
    real_estate_type = Select(driver.find_element_by_id("oss-real-estate-type-buy"))
    real_estate_type.select_by_value("APARTMENT_BUY")
    logger.info("Real estate type: Apartment")

    # Click the result button
    search_btn = driver.find_element_by_css_selector(
        ".oss-main-criterion.oss-button.button-primary.one-whole"
    )
    spans = search_btn.find_elements_by_css_selector("span")
    logger.info(" ".join([x.text for x in spans]))
    search_btn.click()

    # Wait for the page to load
    WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, "pageSelection")))

    return driver.current_url


def parse_result_page(driver: webdriver.Chrome, logger: logging.Logger,
                      subprocess_drivers: List[webdriver.Chrome]):
    """ Extract links to each property page """
    # Wait for the page to load
    WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, "pageSelection")))

    logger.info(f"Crawling result page: {driver.current_url}")
    # Print the header information
    search_head = driver.find_element_by_id("searchHead")\
        .find_element_by_css_selector("h1")
    logger.info(f"Search Head: {search_head.text}")
    property_headings = driver.find_elements_by_css_selector(
        "div#listings ul#resultListItems li.result-list__listing"
    )

    property_links = [
        x.find_element_by_css_selector(
            "a.result-list-entry__brand-title-container"
        ).get_attribute("href")
        for x in property_headings
    ]

    # Extract data from each property listing
    process_property_links(property_links, logger)

    # Find the link for next page to crawl
    try:
        next_link_element = driver.find_element_by_id("pager")\
            .find_element_by_css_selector("a[data-is24-qa='paging_bottom_next']")
        next_link = next_link_element.get_attribute("href")
        global NO_OF_PAGES
        NO_OF_PAGES += 1
        if NO_OF_PAGES < 3:
            RESULT_PAGE_URL.append(next_link)
    except NoSuchElementException:
        logger.info("Last page reached")


def process_property_links(urls_list: List[str], logger: logging.Logger):
    """ Use multiprocessing for property links """
    logger.info(f"No of property URLS to be processed: {len(urls_list)}")
    url_per_process = len(urls_list) // NO_OF_PROCESS
    sub_url_list = urls_list[:url_per_process]
    urls_list = urls_list[url_per_process:]
    process1 = mp.Process(
        target=multiprocessing_property_links,
        args=(sub_url_list, "process1")
    )
    process1.start()

    sub_url_list = urls_list[:url_per_process]
    urls_list = urls_list[url_per_process:]
    process2 = mp.Process(
        target=multiprocessing_property_links,
        args=(sub_url_list, "process2")
    )
    process2.start()

    sub_url_list = urls_list[:url_per_process]
    urls_list = urls_list[url_per_process:]
    process3 = mp.Process(
        target=multiprocessing_property_links,
        args=(sub_url_list, "process3")
    )
    process3.start()

    process4 = mp.Process(
        target=multiprocessing_property_links,
        args=(urls_list, "process4")
    )
    process4.start()

    # Waiting for all the processes to complete
    process1.join()
    process2.join()
    process3.join()
    process4.join()


def multiprocessing_property_links(urls_list: List[str], process_name: str):
    """ This function will be run by the multiprocessing module """
    driver = PROCESS_NAME_DRIVER_MAPPING[process_name]
    for url in urls_list:
        driver.get(url)
        time.sleep(1)
        data = parse_property_page(driver)
        if data:
            CRAWLED_DATA.put(data)


def parse_property_page(driver: webdriver.Chrome):
    """ Extract data from the property page """
    # Some of the pages looks different
    # self.logger.info(f"Crawling property page: {driver.current_url}")
    try:
        main_property_section = driver.find_element_by_css_selector(
            "div.is24-ex-details.main-criteria-headline-size.two-column-layout"
        )
    except NoSuchElementException:
        # self.logger.warning("This page can not be crawled")
        return None

    try:
        name = main_property_section.find_element_by_css_selector("div.criteriagroup h1")
        name = name.text.strip()
    except NoSuchElementException:
        # self.logger.warning("Name not found")
        name = NOT_FOUND

    try:
        address = main_property_section.find_elements_by_css_selector(
            "div.address-block span"
        )
        address = [x.text for x in address]
        address = "\n".join(address)
    except NoSuchElementException:
        # self.logger.warning("Address Not Found")
        address = NOT_FOUND

    # Purpase price section
    try:
        purchase_price = main_property_section.find_element_by_css_selector(
            "div.mainCriteria.flex-item "
            "div.is24qa-kaufpreis.is24-value.font-semibold.is24-preis-value"
        )
        purchase_price = purchase_price.text.strip()
    except NoSuchElementException:
        # self.logger.warning("Purchase Price Not Found")
        purchase_price = NOT_FOUND

    try:
        monthly_price = main_property_section.find_element_by_css_selector(
            "span.monthly-rate-result.monthly-rate-value.padding-top-xs"
        )
        monthly_price = monthly_price.text.strip()
    except NoSuchElementException:
        # self.logger.warning("Monthly Price Not Found")
        monthly_price = NOT_FOUND

    try:
        room = main_property_section.find_element_by_css_selector(
            "div.mainCriteria.flex-item div.is24qa-zi.is24-value.font-semibold"
        )
        room = room.text.strip()
    except NoSuchElementException:
        # self.logger.warning("Room Not Found")
        room = NOT_FOUND

    try:
        living_space = main_property_section.find_element_by_css_selector(
            "div.mainCriteria.flex-item div.is24qa-wohnflaeche-ca.is24-value.font-semibold"
        )
        living_space = living_space.text.strip()
    except NoSuchElementException:
        # self.logger.warning("Living Space Not Found")
        living_space = NOT_FOUND

    # Provider name and phone number section
    try:
        provider_name = main_property_section.find_element_by_css_selector(
            "div.style__truncateChild___2Z9XG.font-semibold"
        )
        provider_name = provider_name.text.strip()
    except NoSuchElementException:
        # self.logger.warning("Provider Name Not Found")
        provider_name = NOT_FOUND

    try:
        phone_no_button = main_property_section.find_element_by_css_selector(
            "button.link-text.padding-none.palm-hide.style__realtorTelephoneNumber___CwmJv")
        phone_no_button.send_keys(Keys.RETURN)
        phone_no_pop_up = driver.find_element_by_id("is24-expose-popup")
        phone_no = phone_no_pop_up.find_element_by_xpath(
            ".//ul[@data-qa='contactPhoneNumbers']//span[@class='font-semibold']"
        )
        phone_no = phone_no.text.strip()
    except NoSuchElementException:
        # self.logger.warning("Phone No Not Found")
        phone_no = NOT_FOUND

    # Property details section
    try:
        amenities = main_property_section.find_elements_by_css_selector(
            "div.criteriagroup.boolean-listing.padding-top-l span.palm-hide"
        )
        amenities = [x.text for x in amenities]
        amenities = ", ".join(amenities)
    except NoSuchElementException:
        # self.logger.warning("Amenities Not Found")
        amenities = NOT_FOUND

    try:
        property_section = main_property_section.find_element_by_css_selector(
            "div.criteriagroup.criteria-group--two-columns"
        )
        property_details = {}
        for info in property_section.find_elements_by_css_selector("dl"):
            property_details[info.find_element_by_css_selector("dt").text] = \
                info.find_element_by_css_selector("dd").text

        property_details = ", ".join(
            [f"{key}: {value}" for key, value in property_details.items()]
        )
    except NoSuchElementException:
        # self.logger.warning("Property details Not Found")
        property_details = NOT_FOUND

    # Cost section
    # There are two div with class grid. But we are interested in the first one
    # Select the div element which is next sibling to h4 element with
    # data-qa=is24qa-kosten-label
    # + is used for next or adjacent sibling
    try:
        cost_section = main_property_section.find_element_by_css_selector(
            "h4[data-qa='is24qa-kosten-label']+div.criteriagroup.one-whole div.grid"
        )
        cost_details = {}
        for info in cost_section.find_elements_by_css_selector("dl"):
            cost_details[info.find_element_by_css_selector("dt").text] = \
                info.find_element_by_css_selector("dd").text

        interested_cost_details = ["Hausgeld", "Provision für Käufer"]
        house_allowance = cost_details.get("Hausgeld", NOT_FOUND)
        commission = cost_details.get("Provision für Käufer", NOT_FOUND)
        cost_details = ", ".join(
            [
                f"{key}: {value}"
                for key, value in cost_details.items()
                if key in interested_cost_details
            ]
        )
    except NoSuchElementException:
        # self.logger.warning("Cost details Not Found")
        house_allowance = NOT_FOUND
        commission = NOT_FOUND
        cost_details = NOT_FOUND

    # Building substance and energy certificate
    try:
        building_section = main_property_section.find_element_by_css_selector(
            "div.criteriagroup.criteria-group--border.criteria-group--two-columns."
            "criteria-group--spacing"
        )
        building_details = {}
        for info in building_section.find_elements_by_css_selector("dl"):
            building_details[info.find_element_by_css_selector("dt").text] = \
                info.find_element_by_css_selector("dd").text

        construction_year = building_details.get("Baujahr", NOT_FOUND)
        building_details = ", ".join(
            [f"{key}: {value}" for key, value in building_details.items()]
        )
    except NoSuchElementException:
        # self.logger.warning("Building information not found")
        construction_year = NOT_FOUND
        building_details = NOT_FOUND

    # Location section
    # Use + for adjacent sibling selector
    try:
        location_info = driver.find_element_by_css_selector(
            "h4.is24qa-lage-label.padding-top-xl.margin-bottom-s+"
            "div.is24-text.margin-bottom.is24-long-text-attribute"
        ).text
    except NoSuchElementException:
        # self.logger.warning("Location Not Found")
        location_info = NOT_FOUND

    return CrawledData(
        name=name,
        address=address,
        pre_processed_purchase_price=purchase_price,
        pre_processed_monthly_price=monthly_price,
        pre_processed_room=room,
        living_space=living_space,
        provider_name=provider_name,
        provider_phone_no=phone_no,
        amenities=amenities,
        property_details=property_details,
        cost_details=cost_details,
        pre_processed_house_allowance=house_allowance,
        commission=commission,
        building_details=building_details,
        pre_processed_construction_year=construction_year,
        location_info=location_info
    )


def write_to_csv():
    with open("output.csv", mode="w") as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=CrawledData.get_fields())
        writer.writeheader()
        while not CRAWLED_DATA.empty():
            data = CRAWLED_DATA.get()
            writer.writerow(data.to_dict())


def main():
    """ Main function """
    sathualab_crawler()
    write_to_csv()


if __name__ == "__main__":
    main()
