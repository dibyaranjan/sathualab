# Define here the models for your spider middleware
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/spider-middleware.html
import random
import os
import itertools

from scrapy import signals
from scrapy.downloadermiddlewares.useragent import UserAgentMiddleware
from fake_useragent import UserAgent

# useful for handling different item types with a single interface
from itemadapter import is_item, ItemAdapter

list_of_languages = ['af', 'sq', 'ar', 'ar-dz', 'ar-bh', 'ar-eg', 'ar-iq', 'ar-jo', 'ar-kw',
         'ar-ma', 'ar-om', 'ar-qa', 'ar-sa', 'ar-sy', 'ar-tn', 'ar-ae', 'ar-ye', 'ar', 'hy', 'as',
         'ast', 'az', 'eu', 'bg', 'be', 'bn', 'bs', 'br', 'bg', 'my', 'ca', 'ch', 'ce', 'zh',
         'zh-hk', 'zh-cn', 'zh-sg', 'zh-tw', 'cv', 'co', 'cr', 'hr', 'cs', 'da', 'nl', 'nl-be',
         'en', 'en-au', 'en-bz', 'en-ca', 'en-ie', 'en-jm', 'en-nz', 'en-ph', 'en-za', 'en-tt',
         'en-gb', 'en-us', 'en-zw', 'eo', 'et', 'fo', 'fa', 'fj', 'fi', 'fr', 'fr-be', 'fr-ca',
         'fr-fr', 'fr-lu', 'fr-mc', 'fr-ch', 'fy', 'fur', 'gd', 'gd-ie', 'gl', 'ka', 'de', 'de-at',
         'de-de', 'de-li', 'de-lu', 'de-ch', 'el', 'gu', 'ht', 'he', 'hi', 'hu', 'is', 'id', 'iu',
         'ga', 'it', 'it-ch', 'ja', 'kn', 'ks', 'kk', 'km', 'ky', 'tlh', 'ko', 'ko-kp', 'ko-kr',
         'la', 'lv', 'lt', 'lb', 'mk', 'ms', 'ml', 'mt', 'mi', 'mr', 'mo', 'nv', 'ng', 'ne', 'no',
         'nb', 'nn', 'oc', 'or', 'om', 'fa', 'fa-ir', 'pl', 'pt', 'pt-br', 'pa', 'pa-in', 'pa-pk',
         'qu', 'rm', 'ro', 'ro-mo', 'ru', 'ru-mo', 'sz', 'sg', 'sa', 'sc', 'gd', 'sd', 'si', 'sr',
         'sk', 'sl', 'so', 'sb', 'es', 'es-ar', 'es-bo', 'es-cl', 'es-co', 'es-cr', 'es-do',
         'es-ec', 'es-sv', 'es-gt', 'es-hn', 'es-mx', 'es-ni', 'es-pa', 'es-py', 'es-pe', 'es-pr',
         'es-es', 'es-uy', 'es-ve', 'sx', 'sw', 'sv', 'sv-fi', 'sv-sv', 'ta', 'tt', 'te', 'th',
         'tig', 'ts', 'tn', 'tr', 'tk', 'uk', 'hsb', 'ur', 've', 'vi', 'vo', 'wa', 'cy', 'xh', 'ji',
         'zu', 'ar-lb', 'ar-ly']
accept_language = []
language_array = []
for i in range(1, len(list_of_languages) - 1):  # range(1, len(lst)-180)
    els = [list(x) for x in itertools.combinations(list_of_languages[:20], i)]
    language_array.extend(els)
for el in language_array:
    var_count = len(el)
    test = ';q={},'.join(el).rstrip(',').format(
        *(round(random.uniform(0, 1), 1) for _ in range(var_count)))
    accept_language.append(test)


class Scout24SpiderMiddleware:
    # Not all methods need to be defined. If a method is not defined,
    # scrapy acts as if the spider middleware does not modify the
    # passed objects.

    @classmethod
    def from_crawler(cls, crawler):
        # This method is used by Scrapy to create your spiders.
        s = cls()
        crawler.signals.connect(s.spider_opened, signal=signals.spider_opened)
        return s

    def process_spider_input(self, response, spider):
        # Called for each response that goes through the spider
        # middleware and into the spider.

        # Should return None or raise an exception.
        return None

    def process_spider_output(self, response, result, spider):
        # Called with the results returned from the Spider, after
        # it has processed the response.

        # Must return an iterable of Request, or item objects.
        for i in result:
            yield i

    def process_spider_exception(self, response, exception, spider):
        # Called when a spider or process_spider_input() method
        # (from other spider middleware) raises an exception.

        # Should return either None or an iterable of Request or item objects.
        pass

    def process_start_requests(self, start_requests, spider):
        # Called with the start requests of the spider, and works
        # similarly to the process_spider_output() method, except
        # that it doesn’t have a response associated.

        # Must return only requests (not items).
        for r in start_requests:
            yield r

    def spider_opened(self, spider):
        spider.logger.info('Spider opened: %s' % spider.name)


class Scout24DownloaderMiddleware:
    # Not all methods need to be defined. If a method is not defined,
    # scrapy acts as if the downloader middleware does not modify the
    # passed objects.

    @classmethod
    def from_crawler(cls, crawler):
        # This method is used by Scrapy to create your spiders.
        s = cls()
        crawler.signals.connect(s.spider_opened, signal=signals.spider_opened)
        return s

    def process_request(self, request, spider):
        # Called for each request that goes through the downloader
        # middleware.

        # Must either:
        # - return None: continue processing this request
        # - or return a Response object
        # - or return a Request object
        # - or raise IgnoreRequest: process_exception() methods of
        #   installed downloader middleware will be called
        return None

    def process_response(self, request, response, spider):
        # Called with the response returned from the downloader.

        # Must either;
        # - return a Response object
        # - return a Request object
        # - or raise IgnoreRequest
        return response

    def process_exception(self, request, exception, spider):
        # Called when a download handler or a process_request()
        # (from other downloader middleware) raises an exception.

        # Must either:
        # - return None: continue processing this exception
        # - return a Response object: stops process_exception() chain
        # - return a Request object: stops process_exception() chain
        pass

    def spider_opened(self, spider):
        spider.logger.info('Spider opened: %s' % spider.name)


class RandomUserAgentMiddleware(UserAgentMiddleware):
    """ Set random user agent for each request """

    def __init__(self, settings, user_agent='Scrapy'):
        super(RandomUserAgentMiddleware, self).__init__()
        self.user_agent = UserAgent()
        user_agent_list_file = settings.get('USER_AGENT_LIST', None)
        self.use_fake_user_agent = settings.get('USE_FAKE_USER_AGENT', False)

        if user_agent_list_file is None:
            # If USER_AGENT_LIST_FILE settings is not set,
            # Use the default USER_AGENT or whatever was
            # passed to the middleware.
            ua = settings.get('USER_AGENT', user_agent)
            self.user_agent_list = [ua]
        else:
            user_agent_list_file = os.path.abspath(os.path.expanduser(user_agent_list_file))
            with open(user_agent_list_file, 'r') as f:
                self.user_agent_list = [line.strip() for line in f.readlines()]

    @classmethod
    def from_crawler(cls, crawler):
        obj = cls(crawler.settings)
        crawler.signals.connect(obj.spider_opened,
                                signal=signals.spider_opened)
        return obj

    def process_request(self, request, spider):
        request.headers.setdefault(
            "User-Agent",
            self.user_agent.random
            if self.use_fake_user_agent
            else random.choice(self.user_agent_list)
        )
        print(f"Inside random user-agent: {request.headers}")


class RandomHeaderLanguageMiddleware(UserAgentMiddleware):
    """ Set random Accept-Language for each request header"""

    def __init__(self, settings, user_agent='Scrapy'):
        super(RandomHeaderLanguageMiddleware, self).__init__()

    @classmethod
    def from_crawler(cls, crawler):
        obj = cls(crawler.settings)
        crawler.signals.connect(obj.spider_opened,
                                signal=signals.spider_opened)
        return obj

    def process_request(self, request, spider):
        print(f"Before Inside random language: {request.headers}")
        request.headers.setdefault(
            "Accept-Language",
            random.choice(accept_language)
        )
        print(f"Inside random language: {request.headers}")
