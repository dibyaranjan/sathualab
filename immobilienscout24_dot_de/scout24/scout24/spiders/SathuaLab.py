import re
import json
from urllib.parse import urljoin
import scrapy
import colorama

from scout24.items import SathuaLabData


class SathualabSpider(scrapy.Spider):
    name = 'SathuaLab'
    allowed_domains = ['immobilienscout24.de']
    not_found = "Not Found"
    
    def __init__(self, *args, **kwargs):
        super(SathualabSpider, self).__init__(*args, **kwargs)
        self.primary_url = "http://immobilienscout24.de/"
        self.cost_api_endpoint = "/baufinanzierung-api/restapi/api/financing/construction/v1.0/" \
                                 "monthlyrate"
        self.cost_url = urljoin(self.primary_url, self.cost_api_endpoint)
        self.js_variable_regex = re.compile(r"IS24.expose\s+=\s+({.*?});", re.MULTILINE | re.DOTALL)
        self.id_regex = re.compile(r"id: (\d+),")
        self.contact_data_regex = re.compile(r"contactData: ({.*}),")
        self.result_pages_crawled = 0
        self.property_pages_crawled = 0
        self.list_of_invalid_property_pages = []
        self.start_url = None
        self.start_parse = None
        if "single_property" in kwargs:
            self.start_url = kwargs["single_property"]
            self.start_parse = self.parse_property_page
        elif "multiple_property" in kwargs:
            self.start_url = kwargs["multiple_property"]
            self.start_parse = self.parse_result_page

    def start_requests(self):
        yield scrapy.Request(url=self.start_url, callback=self.start_parse)

    def parse_result_page(self, response):
        """ Parse result page """
        print(f"\t{colorama.Fore.CYAN}Crawling result page: {response.url}")
        self.result_pages_crawled += 1
        property_headings = response.css(
            "div#listings ul#resultListItems li.result-list__listing"
        )

        property_links = [
            x.css("a.result-list-entry__brand-title-container").attrib["href"]
            for x in property_headings
        ]

        # Extract data from each property listing
        for link in property_links:
            yield response.follow(url=link, callback=self.parse_property_page)

        # Find the link for next page to crawl
        next_link = response.css("#pager")\
            .xpath(".//a[@data-is24-qa='paging_bottom_next']/@href").get()
        if next_link:
            yield response.follow(url=next_link, callback=self.parse_result_page)

    def parse_property_page(self, response):
        """ Parse each property page to extract information """
        self.property_pages_crawled += 1
        self.logger.info(f"Crawling property page: {response.url}")
        print(f"\t{colorama.Fore.LIGHTRED_EX}Crawling property page: {response.url}")
        main_property_section = response.css(
            "div.is24-ex-details.main-criteria-headline-size.two-column-layout"
        )

        if not main_property_section:
            self.list_of_invalid_property_pages.append(response.url)
            return None

        name = main_property_section.css(
            "div.criteriagroup h1::text"
        ).get(default=self.not_found).strip()
        self.logger.debug(f"name: {name}")

        address = main_property_section.css("div.address-block span::text").getall()
        address = "\n".join(address) if address else self.not_found
        self.logger.debug(f"Address: {address}")

        # Room section
        room = main_property_section.css(
            "div.mainCriteria.flex-item div.is24qa-zi.is24-value.font-semibold::text"
        ).get(default=self.not_found).strip()
        self.logger.debug(f"Room: {room}")

        living_space = main_property_section.css(
            "div.mainCriteria.flex-item div.is24qa-wohnflaeche-ca.is24-value.font-semibold::text"
        ).get(default=self.not_found).strip()
        self.logger.debug(f"Living Space: {living_space}")

        # Property details section
        amenities = main_property_section.css(
            "div.criteriagroup.boolean-listing.padding-top-l span.palm-hide::text"
        ).getall()
        amenities = ", ".join(amenities) if amenities else self.not_found
        self.logger.debug(f"Amenities: {amenities}")

        property_section = main_property_section.css(
            "div.criteriagroup.criteria-group--two-columns"
        )
        if property_section:
            property_section = property_section.pop(0)
            property_details = {}
            for info in property_section.css("dl"):
                property_details[info.css("dt::text").get()] = info.css("dd::text").get()

            property_details = ", ".join(
                [f"{key}: {value}" for key, value in property_details.items()]
            )
        else:
            property_details = self.not_found

        self.logger.debug(f"Property details: {property_details}")

        # Cost section
        # There are two div with class grid. But we are interested in the first one
        # Select the div element which is next sibling to h4 element with
        # data-qa=is24qa-kosten-label
        # + is used for next or adjacent sibling
        cost_section = main_property_section.css(
            "h4[data-qa='is24qa-kosten-label']+div.criteriagroup.one-whole div.grid"
        )
        cost_details = {}
        for info in cost_section.css("dl"):
            cost_details[info.css("dt::text").get()] = info.css("dd::text").get()

        house_allowance = cost_details.get("Hausgeld")
        if house_allowance:
            house_allowance = house_allowance.replace("€", "").replace(".", "").replace(",", "")
            house_allowance = float(
                house_allowance.strip()
            )
        else:
            house_allowance = 0.0
        # commission = cost_details.get("Provision für Käufer", self.not_found)
        cost_details = ", ".join(
            [
                f"{key}: {value}"
                for key, value in cost_details.items()
            ]
        )
        self.logger.debug(f"House Allowance: {house_allowance}")
        self.logger.debug(f"Cost details: {cost_details}")

        # Building substance and energy certificate
        building_section = main_property_section.css(
            "div.criteriagroup.criteria-group--border.criteria-group--two-columns."
            "criteria-group--spacing"
        )
        building_details = {}
        for info in building_section.css("dl"):
            building_details[info.css("dt::text").get()] = info.css("dd::text").get()

        # construction_year = building_details.get("Baujahr", self.not_found)
        # object_state = building_details.get("Objektzustand", self.not_found)
        # type_of_heating = building_details.get("Heizungsart", self.not_found)
        building_details = ", ".join(
            [f"{key}: {value}" for key, value in building_details.items()]
        )
        construction_year = building_section.css("dd.is24qa-baujahr::text")\
            .get(default=self.not_found)
        object_state = building_section.css("dd.is24qa-objektzustand::text")\
            .get(default=self.not_found)
        type_of_heating = building_section.css("dd.is24qa-heizungsart::text")\
            .get(default=self.not_found)
        self.logger.debug(f"Construction Year: {construction_year}")
        self.logger.debug(f"Building details: {building_details}")

        # Location section
        # Use + for adjacent sibling selector
        location_info = response.css(
            "h4.is24qa-lage-label.padding-top-xl.margin-bottom-s+"
            "div pre::text"
        ).get(default=self.not_found)
        self.logger.debug(f"Location: {location_info}")

        # Extract contact information from the JavaScript variable
        is24_variable = self.js_variable_regex.search(response.text)
        phone_numbers = []
        contact_person = self.not_found
        expose_id = None
        if is24_variable:
            is24_value = is24_variable.group(1)
            contact_data = json.loads(self.contact_data_regex.search(is24_value).group(1))
            expose_id = self.id_regex.search(is24_value).group(1)

            phone_nombers_data = contact_data.get("phoneNumbers")
            if phone_nombers_data:
                for value in phone_nombers_data.values():
                    phone_numbers.append(value["contactNumber"])

            contact_person_data = contact_data.get("contactPerson")
            if contact_person_data:
                salutation = contact_person_data.get("salutationAndTitle", "")
                first_name = contact_person_data.get("firstName", "")
                last_name = contact_person_data.get("lastName", "")
                contact_person = f"{salutation} {first_name} {last_name}"

        contact_person_number = ", ".join(phone_numbers) if phone_numbers else self.not_found
        self.logger.debug(f"Contact Person: {contact_person}")
        self.logger.debug(f"Contact Number: {contact_person_number}")
        self.logger.debug(f"Expose Id: {expose_id}")

        crawled_data = SathuaLabData(
            name=name,
            address=address,
            room=room,
            living_space=living_space,
            provider_name=contact_person,
            provider_phone_no=contact_person_number,
            amenities=amenities,
            property_details=property_details,
            cost_details=cost_details,
            house_allowance=house_allowance,
            building_details=building_details,
            construction_year=construction_year,
            object_state=object_state,
            type_of_heating=type_of_heating,
            location_info=location_info
        )

        body = {"exposeId": expose_id}
        yield scrapy.http.FormRequest(
            url=self.cost_url,
            formdata=body,
            method="GET",
            cb_kwargs={"crawled_data": crawled_data},
            callback=self.parse_cost_json_response
        )

    def parse_cost_json_response(self, response, crawled_data):
        json_data = response.json()
        crawled_data["purchase_price"] = float(json_data.get("purchasePrice", 0.0))
        crawled_data["monthly_price"] = float(json_data.get("monthlyRate", 0.0))
        crawled_data["commission"] = float(
            json_data.get("additionalCosts", {}).get("brokerCommissionPercentage", 0.0)
        )

        self.logger.debug(f"Purchase price: {crawled_data['purchase_price']}")
        self.logger.debug(f"Monthly price: {crawled_data['monthly_price']}")
        self.logger.debug(f"Commission: {crawled_data['commission']}")
        yield crawled_data
