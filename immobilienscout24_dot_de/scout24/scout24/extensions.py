"""
File:           extensions.py
Author:         Dibyaranjan Sathua
Created on:     19/07/20, 7:46 pm
"""
from scrapy import signals
from scrapy.exceptions import NotConfigured
import colorama
import time


class SpiderSummary:
    """ Print a summary after the spider is closed """
    def __init__(self):
        self.start_time = None

    @classmethod
    def from_crawler(cls, crawler):
        # first check if the extension should be enabled and raise NotConfigured otherwise
        if not crawler.settings.getbool('SPIDER_SUMMARY_ENABLED'):
            raise NotConfigured

        # instantiate the extension object
        extension = cls()

        # connect the extension object to signals
        crawler.signals.connect(extension.spider_opened, signal=signals.spider_opened)
        crawler.signals.connect(extension.spider_closed, signal=signals.spider_closed)

        # return the extension object
        return extension

    def spider_opened(self, spider):
        print(f"\n{colorama.Fore.GREEN}Solution by SathuaLab (www.sathualab.com)")
        print(f"Crawling data from {spider.primary_url}")
        self.start_time = time.time()

    def spider_closed(self, spider):
        total_time = time.strftime(
            "%H hours %M mins %S secs",
            time.gmtime(time.time() - self.start_time)
        )
        print(f"\n\n{colorama.Fore.MAGENTA}=========================== Summary ================================")
        print(f"\t{colorama.Fore.GREEN}Time taken: {total_time}")
        print(f"\t{colorama.Fore.GREEN}Result pages crawled: {spider.result_pages_crawled}")
        print(f"\t{colorama.Fore.GREEN}Property pages crawled: {spider.property_pages_crawled}")
        print(f"\t{colorama.Fore.GREEN}Bad property pages: {len(spider.list_of_invalid_property_pages)}")
        if len(spider.list_of_invalid_property_pages):
            print(f"\t{colorama.Fore.GREEN}Bad property links")
            for link in spider.list_of_invalid_property_pages:
                print(f"\t\t--> {link}")

        print(f"{colorama.Fore.MAGENTA}======================================================================\n\n")
        print(colorama.Fore.RESET)
