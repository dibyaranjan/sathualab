# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class SathuaLabData(scrapy.Item):
    """ Data class to store crawled data """
    name = scrapy.Field()
    address = scrapy.Field()
    purchase_price = scrapy.Field()
    monthly_price = scrapy.Field()
    room = scrapy.Field()
    living_space = scrapy.Field()
    provider_name = scrapy.Field()
    provider_phone_no = scrapy.Field()
    amenities = scrapy.Field()
    property_details = scrapy.Field()
    cost_details = scrapy.Field()
    house_allowance = scrapy.Field()
    commission = scrapy.Field()
    building_details = scrapy.Field()
    construction_year = scrapy.Field()
    object_state = scrapy.Field()
    type_of_heating = scrapy.Field()
    energy_efficieny_class = scrapy.Field()
    location_info = scrapy.Field()
