"""
File:           start_crawling.py
Author:         Dibyaranjan Sathua
Created on:     25/07/20, 4:45 pm
"""
import yaml
from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings


def main():
    """ Main function for crawling """
    single_property = "single_property"
    multiple_property = "multiple_property"
    # Loading config.yaml file
    with open("config.yaml", mode="r") as infile:
        config = yaml.full_load(infile)

    kwargs = dict()
    if single_property in config["input"]:
        kwargs[single_property] = config["input"][single_property]
        # When the input is exposeId
        if type(kwargs[single_property]) == int:
            kwargs[single_property] = f"https://www.immobilienscout24.de/" \
                                      f"expose/{kwargs[single_property]}/"

    elif multiple_property in config["input"]:
        kwargs[multiple_property] = config["input"][multiple_property]

    output_filename = config["output"]["csv"]
    feeds_settings = {
        output_filename: {
            "format": "csv"
        }
    }

    settings = get_project_settings()
    settings["FEEDS"] = feeds_settings

    process = CrawlerProcess(settings=settings)
    # Sending url to SathuaLab spider class
    process.crawl('SathuaLab', **kwargs)
    process.start()


if __name__ == "__main__":
    main()
