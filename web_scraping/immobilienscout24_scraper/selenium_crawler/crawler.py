"""
File:           crawler.py
Author:         Dibyaranjan Sathua
Created on:     07/07/20, 3:54 am
"""
import time
import os
import csv

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import NoSuchElementException

from data import CrawlData


OUTPUT_DIR = os.path.join(
    os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(__file__)))),
    "output",
    "web_scraping"
)

OUTPUT_CSV_FILE = os.path.join(OUTPUT_DIR, "immobilienscout24.csv")


class SathuaLabCrawler:
    """ Crawl a URL to extract the data """
    URL = "https://www.immobilienscout24.de/Suche/de/berlin/berlin/mitte-mitte/wohnung-kaufen?enteredFrom=result_list"
    # URL = "https://www.immobilienscout24.de/expose/116912923?referrer=RESULT_LIST_LISTING&navigationServiceUrl=%2FSuche%2Fcontroller%2FexposeNavigation%2Fnavigate.go%3FsearchUrl%3D%2FSuche%2Fde%2Fberlin%2Fberlin%2Fmitte-mitte%2Fwohnung-kaufen%3FenteredFrom%253Dresult_list%26exposeId%3D116912923&navigationHasPrev=true&navigationHasNext=true&navigationBarType=RESULT_LIST&searchId=f5b2fa21-0213-3121-9981-0b00144a478a&searchType=district#/"
    URL = "https://www.immobilienscout24.de/Suche/de/berlin/berlin/mitte-mitte/wohnung-kaufen?pagenumber=2"
    def __init__(self):
        """ Initialize variables """
        self.options = webdriver.ChromeOptions()
        self.options.add_argument('--ignore-certificate-errors')
        self.options.add_argument('--incognito')
        self.options.add_argument('--headless')
        self.driver = None
        self.response = None
        self.crawl_data = []

    def __enter__(self):
        """ Context manager enter function """
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        """ Context manager exit function """
        if self.driver:
            self.driver.close()

    def crawl(self):
        self.driver = webdriver.Chrome(
            "/Users/dibyaranjan/sathualab/lib/chromedriver_mac",
            options=self.options
        )
        self.driver.get(self.URL)
        time.sleep(1)

        self._parse_result_page()

    def _parse_result_page(self):
        """ Extract links to each property page """
        import pdb; pdb.set_trace()
        property_headings = self.driver.find_elements_by_css_selector(
            "div#listings ul#resultListItems li.result-list__listing.result-list__listing--xl"
        )

        property_links = [
            x.find_element_by_css_selector(
                "a.result-list-entry__brand-title-container.vertical-center"
            ).get_attribute("href")
            for x in property_headings
        ]
        for link in property_links:
            self.driver.get(link)
            time.sleep(1)
            data = self._parse_property_page()
            if data:
                self.crawl_data.append(data)

    def _parse_property_page(self):
        """ Extract data from the property page """
        # Some of the pages looks different
        print(f"Crawling {self.driver.current_url}")
        try:
            main_property_section = self.driver.find_element_by_css_selector(
                "div.is24-ex-details.main-criteria-headline-size.two-column-layout"
            )
        except NoSuchElementException as err:
            return None

        name = main_property_section.find_element_by_css_selector("div.criteriagroup h1")
        name = name.text.strip() if name else "Name Not Found"

        address = main_property_section.find_elements_by_css_selector(
            "div.address-block span"
        )
        address = [x.text for x in address]
        address = "\n".join(address) if address else "Address Not Found"

        # Purpase price section
        purchase_price = main_property_section.find_element_by_css_selector(
            "div.mainCriteria.flex-item "
            "div.is24qa-kaufpreis.is24-value.font-semibold.is24-preis-value"
        )
        purchase_price = purchase_price.text.strip() \
            if purchase_price \
            else "Purchase Price Not Found"

        monthly_price = main_property_section.find_element_by_css_selector(
            "span.monthly-rate-result.monthly-rate-value.padding-top-xs"
        )
        monthly_price = monthly_price.text.strip() if monthly_price else "Monthly Price Not Found"

        room = main_property_section.find_element_by_css_selector(
            "div.mainCriteria.flex-item div.is24qa-zi.is24-value.font-semibold"
        )
        room = room.text.strip() if room else "Room Not Found"

        living_space = main_property_section.find_element_by_css_selector(
            "div.mainCriteria.flex-item div.is24qa-wohnflaeche-ca.is24-value.font-semibold"
        )
        living_space = living_space.text.strip() if living_space else "Living Space Not Found"

        # Provider name and phone number section
        provider_name = main_property_section.find_element_by_css_selector(
            "div.style__truncateChild___2Z9XG.font-semibold"
        )
        provider_name = provider_name.text.strip() if provider_name else "Provider Name Not Found"

        phone_no_button = main_property_section.find_element_by_css_selector(
            "button.link-text.padding-none.palm-hide.style__realtorTelephoneNumber___CwmJv")
        phone_no_button.send_keys(Keys.RETURN)
        phone_no_pop_up = self.driver.find_element_by_id("is24-expose-popup")
        phone_no = phone_no_pop_up.find_element_by_xpath(
            ".//ul[@data-qa='contactPhoneNumbers']//span[@class='font-semibold']"
        )
        phone_no = phone_no.text.strip() if phone_no else "Phone No Not Found"

        # Property details section
        amenities = main_property_section.find_elements_by_css_selector(
            "div.criteriagroup.boolean-listing.padding-top-l span.palm-hide"
        )
        amenities = [x.text for x in amenities]
        amenities = ", ".join(amenities) if amenities else "Amenities Not Found"

        property_section = main_property_section.find_element_by_css_selector(
            "div.criteriagroup.criteria-group--two-columns"
        )
        property_details = {}
        for info in property_section.find_elements_by_css_selector("dl"):
            property_details[info.find_element_by_css_selector("dt").text] = \
                info.find_element_by_css_selector("dd").text

        property_details = ", ".join(
            [f"{key}: {value}" for key, value in property_details.items()]
        )

        # Cost section
        # There are two div with class grid. But we are interested in the first one
        # Select the div element which is next sibling to h4 element with
        # data-qa=is24qa-kosten-label
        # + is used for next or adjacent sibling
        cost_section = main_property_section.find_element_by_css_selector(
            "h4[data-qa='is24qa-kosten-label']+div.criteriagroup.one-whole div.grid"
        )
        cost_details = {}
        for info in cost_section.find_elements_by_css_selector("dl"):
            cost_details[info.find_element_by_css_selector("dt").text] = \
                info.find_element_by_css_selector("dd").text

        # interested_cost_details = ["Hausgeld", "Provision für Käufer"]
        cost_details = ", ".join(
            [f"{key}: {value}" for key, value in cost_details.items()]
        )

        # Building substance and energy certificate
        building_section = main_property_section.find_element_by_css_selector(
            "div.criteriagroup.criteria-group--border.criteria-group--two-columns."
            "criteria-group--spacing"
        )
        building_details = {}
        for info in building_section.find_elements_by_css_selector("dl"):
            building_details[info.find_element_by_css_selector("dt").text] = \
                info.find_element_by_css_selector("dd").text

        building_details = ", ".join(
            [f"{key}: {value}" for key, value in building_details.items()]
        )

        # Location section
        # Use + for adjacent sibling selector
        try:
            location_info = self.driver.find_element_by_css_selector(
                "h4.is24qa-lage-label.padding-top-xl.margin-bottom-s+"
                "div.is24-text.margin-bottom.is24-long-text-attribute"
            ).text
        except NoSuchElementException as err:
            location_info = "Location Not Found"

        return CrawlData(
            name=name,
            address=address,
            purchase_price=purchase_price,
            monthly_price=monthly_price,
            room=room,
            living_space=living_space,
            provider_name=provider_name,
            provider_phone_no=phone_no,
            amenities=amenities,
            property_details=property_details,
            cost_details=cost_details,
            building_details=building_details,
            location_info=location_info
        )

    def write_to_csv(self):
        with open(OUTPUT_CSV_FILE, mode="w") as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=CrawlData.get_fields())
            writer.writeheader()
            for data in self.crawl_data:
                writer.writerow(data.to_dict())


if __name__ == "__main__":
    with SathuaLabCrawler() as crawler:
        crawler.crawl()
        crawler.write_to_csv()