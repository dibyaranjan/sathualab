"""
File:           data.py
Author:         Dibyaranjan Sathua
Created on:     07/07/20, 9:18 pm
"""
from dataclasses import dataclass, fields


@dataclass()
class CrawlData:
    """ Data class to store crawled data """
    name: str
    address: str
    purchase_price: str
    monthly_price: str
    room: str
    living_space: str
    provider_name: str
    provider_phone_no: str
    amenities: str
    property_details: str
    cost_details: str
    building_details: str
    location_info: str

    @staticmethod
    def get_fields():
        """ Return the data class fields name """
        return [x.name for x in fields(CrawlData)]

    def to_dict(self):
        """ Convert data class to dict """
        class_fields = CrawlData.get_fields()
        return {x: getattr(self, x) for x in class_fields}
