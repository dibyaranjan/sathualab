import scrapy


class Immobilienscout24Spider(scrapy.Spider):
    name = "immobilienscout24"
    allowed_domains = ["immobilienscout24.de"]
    # start_urls = ['http://immobilienscout24.de/']
    start_urls = ["https://www.immobilienscout24.de/Suche/de/berlin/berlin/mitte-mitte/wohnung-kaufen?enteredFrom=result_list"]

    def parse(self, response):
        """ Parse the result page to get the link for individual property """
        property_list = response.css("a.result-list-entry__brand-title-container.vertical-center")
        for each_property in property_list:
            property_url = response.urljoin(each_property.xpath("@href").get())
            yield scrapy.Request(url=property_url, callback=self.parse_property)

    def parse_property(self, response):
        """ Parse each property page to get information """
        main_property_section = response.css(
            "div.is24-ex-details.main-criteria-headline-size.two-column-layout"
        )
        name = main_property_section.css("div.criteriagroup h1::text")\
            .get(default="Property Name Not Found")

        address = main_property_section.css("div.address-block span::text").getall()
        address = "\n".join(address) if address else "Address Not Found"

        amenities = main_property_section\
            .css("div.criteriagroup.boolean-listing.padding-top-l span.palm-hide::text")\
            .getall()
        amenities = ", ".join(amenities) if amenities else "Amenities Not Found"

        category = main_property_section.css("div.criteriagroup.criteria-group--two-columns")
        property_details = {}
        for info in category[0].css("dl"):
            property_details[info.css('dt::text').get()] = info.css('dd::text').get()

        property_details = str(property_details)

        purchase_price = main_property_section\
            .css("div.is24qa-kaufpreis.is24-value.font-semibold.is24-preis-value::text")\
            .get(default="Purchase Price Not Found")\
            .strip()





