# -*- coding: utf-8 -*-
import scrapy


class JournalbotSpider(scrapy.Spider):
    name = 'journalbot'
    allowed_domains = ['www.openaccessjournals.com/']
    start_urls = ['http://www.openaccessjournals.com//']

    def parse(self, response):
        # Extracting the contents using CSS selectors
        # pdf_files = response.css("a.radius-50::attr(href)").re(r".*\.pdf$")
        # for file in pdf_files:
        #     scraped_info = {
        #         "file_urls": [file]
        #     }
        #     yield scraped_info
        links = response.css("a::attr(href)").getall()
        for link in links:
            scraped_info = {
                "link": link
            }
            yield scraped_info
