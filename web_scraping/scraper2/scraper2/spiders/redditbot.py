# -*- coding: utf-8 -*-
import scrapy


class RedditbotSpider(scrapy.Spider):
    name = 'redditbot'
    allowed_domains = ['www.reddit.com/r/gameofthrones/']
    start_urls = ['http://www.reddit.com/r/gameofthrones//']

    def parse(self, response):
        # Extracting the contents using CSS selectors
        titles = response.css("h3._eYtD2XCVieq6emjKBH3m::text").getall()
        votes = response.css("div._1rZYMD_4xY3gRcSS3p8ODO::text").getall()
        times = response.css("a._3jOxDPIQ0KaOWpzvSQo-1s::text").getall()
        comments = response.css("span.FHCV02u6Cp2zYL0fhQPsO::text").getall()

        for item in zip(titles, votes, times, comments):
            # Create a dictionary to store the scraped info
            scraped_info = {
                "title": item[0],
                "vote": item[1],
                "time": item[2],
                "comment": item[3]
            }

            yield scraped_info
