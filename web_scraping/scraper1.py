"""
File:           scraper1.py
Author:         Dibyaranjan Sathua
Created on:     03/06/20, 1:49 am

Scraping https://www.nfl.com/schedules/2020/REG1/ and save it to a CSV file. Extract date time
for different match and save it to a CSV file.

HTML Web Formatter: webformatter.com/
Output: output/scraper1.csv
"""
import time
import os
import csv
from dataclasses import dataclass

import requests
from bs4 import BeautifulSoup
from selenium import webdriver


OUTPUT_DIR = os.path.join(
    os.path.dirname(os.path.dirname(__file__)),
    "output",
    "web_scraping"
)

OUTPUT_CSV_FILE = os.path.join(OUTPUT_DIR, "scraper1.csv")


@dataclass
class Data:
    date: str = ""
    time: str = ""
    timezone: str = ""
    channel: str = ""
    team1: str = ""
    team2: str = ""

    def to_dict(self):
        return {
            "date": self.date,
            "time": self.time,
            "timezone": self.timezone,
            "channel": self.channel,
            "team1": self.team1,
            "team2": self.team2
        }


class Scraper:
    """ Scrap website """
    URL = "https://www.nfl.com/schedules/2020/REG1/"

    def __init__(self):
        self.response = None
        self.headers = {
            "user-agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_5) AppleWebKit/537.36 "
                          "(KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36"
        }
        self.soup = None
        self.results = None
        self.web_data = []

    def scrap(self):
        self.response = requests.get(self.URL, headers=self.headers)
        self.soup = BeautifulSoup(self.response.text, "html.parser")
        all_sections = self.soup.find_all("section", {
            "class": "d3-l-grid--outer d3-l-section-row nfl-o-matchup-group"
        })
        # print(all_sections[2])
        for section in all_sections:
            date = section.find("h2", {
                "class": "d3-o-section-title"
            })
            # print(date.text.strip())
            all_games = section.find_all("div", {
                "class": "nfl-c-matchup-strip nfl-c-matchup-strip--pre-game"
            })
            for game in all_games:
                data = Data()
                game_time = game.find("span", {
                    "class": "nfl-c-matchup-strip__date-time"
                })
                game_timezone = game.find("span", {
                    "class": "nfl-c-matchup-strip__date-timezone"
                })
                channel = game.find("p", {
                    "class": "nfl-c-matchup-strip__networks"
                })
                teams = game.find_all("span", {
                    "class": "nfl-c-matchup-strip__team-fullname"
                })

                data.date = date.text.strip()
                data.time = game_time.text.strip()
                data.timezone = game_timezone.text.strip()
                data.channel = channel.text.strip()
                data.team1 = teams[0].text.strip()
                data.team2 = teams[1].text.strip()
                self.web_data.append(data)

    def print_web_data(self):
        for data in self.web_data:
            print(f"{data.date} {data.time} {data.timezone} {data.channel} "
                  f"{data.team1} {data.team2}")

    def write_to_csv(self):
        dummy_data = Data()
        fieldnames = list(dummy_data.to_dict().keys())
        with open(OUTPUT_CSV_FILE, mode="w") as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            writer.writeheader()
            for data in self.web_data:
                writer.writerow(data.to_dict())


if __name__ == "__main__":
    scraper = Scraper()
    scraper.scrap()
    scraper.write_to_csv()
