# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

from dataclasses import dataclass, field


# Dataclass is only supported in scrapy 2.2.0
@dataclass()
class Scraper4Item:
    name: str = field(default="")
    bio: str = field(default="")
    created_date: str = field(default="")
    twitter_link: str = field(default="")

