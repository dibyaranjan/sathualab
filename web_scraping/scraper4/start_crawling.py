#!/usr/bin/env python3
"""
File:           start_crawling.py
Author:         Dibyaranjan Sathua
Created on:     05/07/20, 9:11 pm

Usage:  ./start_crawling.py brokenbottleboy
"""
import sys

from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print(f"Please provide one user name to scrape")
        print(f"./start_crawling brokenbottleboy")
        sys.exit(1)

    settings = get_project_settings()
    process = CrawlerProcess(settings=settings)
    # Sending user name to medium spider class
    process.crawl('medium', f"@{sys.argv[1]}")
    process.start()
