"""
File:           scraper3.py
Author:         Dibyaranjan Sathua
Created on:     02/07/20, 12:21 am
"""
from dataclasses import dataclass, field
from typing import List
import os
import csv

import requests
from bs4 import BeautifulSoup


OUTPUT_DIR = os.path.join(
    os.path.dirname(os.path.dirname(__file__)),
    "output",
    "web_scraping"
)

OUTPUT_CSV_FILE = os.path.join(OUTPUT_DIR, "scraper3.csv")


@dataclass()
class County:
    name: str = field(init=False)
    url: str = field(init=False)
    communities: str = field(init=False)
    state: str = field(init=False)


@dataclass()
class CountyCompanyData:
    county: County = field(init=False)
    name: str = field(init=False)
    url: str = field(init=False)
    provides: str = field(init=False)
    rating: str = field(init=False)
    reviews: str = field(init=False)
    description: str = field(init=False)

    def to_dict(self):
        return {
            "State": self.county.state,
            "County": self.county.name,
            "Company": self.name,
            "Service": self.provides,
            "Reviews": self.reviews,
            "Rating": self.rating,
            "Description": self.description
        }


class Scraper:
    """ Scrap website """
    URL = "https://www.caring.com/"

    def __init__(self):
        self.response = None
        self.headers = {
            "user-agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_5) AppleWebKit/537.36 "
                          "(KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36"
        }
        self.soup = None
        self.results = None
        self.care_type = "in-home-care"
        self.location = "washington"
        self.search_url = f"{self.URL}local/search?utf8=%E2%9C%93&type={self.care_type}&" \
                          f"location={self.location}"
        self.web_data: List[CountyCompanyData] = []

    def scrap(self):
        self.response = requests.get(self.search_url, headers=self.headers)
        # Search URL will redirect to the result page url
        redirected_url = self.response.url
        self.response = requests.get(redirected_url, headers=self.headers)
        self.soup = BeautifulSoup(self.response.text, "html.parser")
        counties_section = self.soup.find("section", attrs={"id": "counties"})
        counties = counties_section.find_all("div", attrs={"class": "lrtr-list-item"})
        for county in counties:
            county_data = County()
            subtitle1_div = county.find("div", attrs={"class": "text-subtitle1"})
            a_element = subtitle1_div.find("a")
            county_data.url = a_element["href"]
            county_data.name = a_element.text.strip()
            subtitle2_div = county.find("div", attrs={"class": "text-subtitle2"})
            county_data.communities = subtitle2_div.text.strip()
            county_data.state = self.location

            # Scrap each county
            self.response = requests.get(county_data.url, headers=self.headers)
            self.soup = BeautifulSoup(self.response.text, "html.parser")
            search_results = self.soup.find_all("div", attrs={"class": "search-result"})
            for result in search_results:
                county_company_data = CountyCompanyData()
                county_company_data.county = county_data
                details = result.find("div", attrs={"class": "details"})
                company = details.find("h3").find("a")
                county_company_data.name = company.text.strip()
                county_company_data.url = company["href"]
                provides = details.find("div", attrs={"class": "provides"})
                county_company_data.provides = provides.text.strip()
                reviews = details.find("div", attrs={"class": "reviews"})
                print(f"{county_company_data.county.name} {county_company_data.name}")
                if reviews:
                    rating = reviews.find("input", attrs={"class": "rating"})
                    if rating:
                        county_company_data.rating = rating["value"].strip()
                    else:
                        county_company_data.rating = "Not Present"

                    no_of_reviews = reviews.find("a", attrs={"class": "hidden-xs"})
                    if no_of_reviews:
                        county_company_data.reviews = no_of_reviews.text.strip()
                    else:
                        county_company_data.reviews = "Not Present"
                else:
                    county_company_data.rating = "Not Present"
                    county_company_data.reviews = "Not Present"

                self.response = requests.get(county_company_data.url, headers=self.headers)
                if self.response.status_code == requests.codes.ok:
                    self.soup = BeautifulSoup(self.response.text, "html.parser")
                    description_section = self.soup.find(
                        "section",
                        attrs={"class": "anchored", "id": "description"}
                    )
                    county_company_data.description = description_section.text
                else:
                    county_company_data.description = "Not Present"

                self.web_data.append(county_company_data)

    def print_web_data(self):
        print("Printing web data ...")
        for data in self.web_data:
            print(f"{data.county.name} {data.name} {data.provides} {data.rating} "
                  f"{data.reviews} {data.description}")
            # if data.name == "Clark County":
            #     print(f"{data.name} {data.company_name} {data.provides} {data.rating} "
            #           f"{data.reviews} {data.description}")

    def write_to_csv(self):
        print("Writing data to csv file...")
        fieldnames = list(self.web_data[0].to_dict().keys())
        with open(OUTPUT_CSV_FILE, mode="w") as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            writer.writeheader()
            for data in self.web_data:
                writer.writerow(data.to_dict())


if __name__ == "__main__":
    scraper = Scraper()
    scraper.scrap()
    # scraper.print_web_data()
    scraper.write_to_csv()
